//
//  ViewController.swift
//  StrechyHeader
//
//  Created by Reddy  on 3/7/20.
//  Copyright © 2020 Reddy . All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    var Data = [["Name":"PrahladReddy","Date":"03/07/2020","TutorialImageName":"Crashlytics"],
                ["Name":"Nallamilli","Date":"03/06/2020","TutorialImageName":"Snackbar"]]
    
    var headerView : UIView!
    var newHeaderLayer : CAShapeLayer!
    
    private let headerheight : CGFloat = 420
    private let headerCut : CGFloat = 50
            

    override func viewDidLoad() {
        super.viewDidLoad()
        self.UpdateView()
    }
    
    func UpdateView() {
        tableView.backgroundColor = UIColor.white
        headerView = tableView.tableHeaderView
        tableView.tableHeaderView = nil
        tableView.rowHeight = UITableView.automaticDimension
        tableView.addSubview(headerView)
        
        newHeaderLayer = CAShapeLayer()
        newHeaderLayer.fillColor = UIColor.black.cgColor
        headerView.layer.mask = newHeaderLayer
        
        let newHeight = headerheight - headerCut / 2
        tableView.contentInset = UIEdgeInsets(top: newHeight, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -newHeight)
        
        self.SetUpNewView()
    }
    
    func SetUpNewView() {
        let newHeight = headerheight - headerCut / 2
        var getHeaderFrame = CGRect(x: 0, y: -newHeight, width: tableView.bounds.width, height: headerheight)
        if tableView.contentOffset.y < newHeight {
            getHeaderFrame.origin.y = tableView.contentOffset.y
            getHeaderFrame.size.height = -tableView.contentOffset.y + headerCut / 2
        }
        
        headerView.frame = getHeaderFrame
        let cutDirection = UIBezierPath()
        cutDirection.move(to: CGPoint(x: 0, y: 0))
        cutDirection.addLine(to: CGPoint(x: getHeaderFrame.width, y: 0))
        cutDirection.addLine(to: CGPoint(x: getHeaderFrame.width, y: getHeaderFrame.height))
        cutDirection.addLine(to: CGPoint(x: 0, y: getHeaderFrame.height - headerCut))
        newHeaderLayer.path = cutDirection.cgPath
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.tableView.decelerationRate = UIScrollView.DecelerationRate.fast
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.SetUpNewView()
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // Tableview Methods
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 272
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 10
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Data.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell
        let indexPath = Data[indexPath.row]
        cell.dateLabel.text = indexPath["Date"]
        cell.tutorialName.text = indexPath["Name"]
        cell.profileImage.image = UIImage(named: indexPath["TutorialImageName"]!)
        return cell
    }
}

