//
//  TableViewCell.swift
//  StrechyHeader
//
//  Created by Reddy  on 3/7/20.
//  Copyright © 2020 Reddy . All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var tutorialName: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
}
